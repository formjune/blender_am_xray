# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# https://bitbucket.org/formjune/blender_am_xray


import bpy
import mathutils


bl_info = {
    "name": "AM 1D Xray++",
    "author": "Andrey Menshikovn, Paul Kotelevets aka 1D_Inc (concept design)",
    "version": ("1", "0", "6"),
    "blender": (2, 7, 9),
    "location": "View3D > Tool Shelf > 1D > AM1D",
    "description": "AM 1D script pack",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Mesh"}


class BlenderParam(object):
    """wrapper for blender global options"""

    def __init__(self, name):
        self.name = name

    def __call__(self):
        return self.get()

    def get(self):
        return bpy.context.scene.am_xray_settings.__getattribute__(self.name)

    def set(self, value):
        bpy.context.scene.am_xray_settings.__setattribute__(self.name, value)


class Settings(bpy.types.PropertyGroup):
    xray_threshold = bpy.props.FloatProperty(default=1e-2, min=1e-3, precision=3, step=1)
    xray_source = bpy.props.PointerProperty(name="object", type=bpy.types.Object)
    xray_max = bpy.props.FloatProperty(default=1, min=1e-3, precision=3, step=1000)
    xray_min = bpy.props.FloatProperty(default=.1, min=0, precision=3, step=1000)
    xray_max_auto = bpy.props.BoolProperty(default=False)
    xray_component = bpy.props.EnumProperty(
        items=[
            ("FACE", "face", ""),
            ("FACE SMOOTH", "face smooth", ""),
            ("VERTEX", "vertex", "")
        ], default="FACE")
    xray_method = bpy.props.EnumProperty(
        items=[
            ("NORMAL", "normal", ""),
            ("CLOSEST", "closest", "")
        ], default="CLOSEST")
    xray_color_max = bpy.props.FloatVectorProperty(subtype="COLOR", default=(.107, .006, .381), min=0, max=1)
    xray_color_min = bpy.props.FloatVectorProperty(subtype="COLOR", default=(1, .216, .051), min=0, max=1)
    xray_color_null = bpy.props.FloatVectorProperty(subtype="COLOR", default=(.026, .604, .117), min=0, max=1)
    xray_color_select = bpy.props.FloatVectorProperty(subtype="COLOR", default=(1, .723, .184), min=0, max=1)


class Xray(bpy.types.Operator):

    bl_idname = "mesh.am1dxr_xray_plus"
    bl_label = "Xray ++"
    bl_options = {'REGISTER', 'UNDO'}

    max = BlenderParam("xray_max")
    min = BlenderParam("xray_min")
    max_auto = BlenderParam("xray_max_auto")
    method = BlenderParam("xray_method")
    color_max = BlenderParam("xray_color_max")
    color_min = BlenderParam("xray_color_min")
    color_null = BlenderParam("xray_color_null")
    color_select = BlenderParam("xray_color_select")
    source = BlenderParam("xray_source")
    threshold = BlenderParam("xray_threshold")
    component = BlenderParam("xray_component")

    def execute(self, context):

        # reset selection
        bpy.ops.object.mode_set(mode="OBJECT")
        active = context.active_object
        for face in active.data.edges:
            face.select = False
        for edge in active.data.edges:
            edge.select = False
        for vertex in active.data.vertices:
            vertex.select = False

        # match matrices
        target = self.source() or active
        if target != active:
            for vertex in target.data.vertices:
                vertex.co = active.matrix_world.inverted() * (target.matrix_world * vertex.co)
            target.matrix_world = active.matrix_world

        # create color layer
        if not active.data.vertex_colors:
            active.data.vertex_colors.new()

        # gather distances and generate colors
        data = []
        if self.component() == "VERTEX":
            if target != active:
                for vertex in active.data.vertices:
                    data.append(self._projectTarget(vertex.co, vertex.normal, target))
            else:
                link_faces = [set() for v in active.data.vertices]
                for face in active.data.polygons:
                    for vertex in face.vertices:
                        link_faces[vertex].add(face.index)
                for vertex in active.data.vertices:
                    data.append(self._projectSource(vertex.co, vertex.normal, link_faces[vertex.index]))
            self._paintVertices(self._findColors(data, active.data.vertices))

        else:
            if target != active:
                for face in active.data.polygons:
                    data.append(self._projectTarget(face.center, face.normal, target))

            else:
                for face in active.data.polygons:
                    data.append(self._projectSource(face.center, face.normal, {face.index}))
            colors = self._findColors(data, active.data.polygons)

            if self.component() == "FACE SMOOTH":
                colors_vertex = [[] for v in active.data.vertices]
                for color, face in zip(colors, active.data.polygons):
                    for vertex in face.vertices:
                        colors_vertex[vertex].append(color)
                self._paintVertices([sum(color, mathutils.Color()) / len(color) for color in colors_vertex])
            else:
                self._paintFaces(colors)

        bpy.ops.object.mode_set(mode="VERTEX_PAINT")
        return {"FINISHED"}

    def _findColors(self, data, components):
        """find colors and select components"""
        if self.max_auto():
            try:
                max_length = max([p for p in data if p is not None])
            except ValueError:
                self.report({"ERROR"}, "can't find max")
                return [], []
        else:
            max_length = self.max()
        min_length = self.min()
        max_color = self.color_max()
        min_color = self.color_min()
        null_color = self.color_null()
        select_color = self.color_select()
        colors = []
        for value, component in zip(data, components):
            if value is None:
                colors.append(null_color)
            elif value < min_length:
                colors.append(select_color)
            elif value > max_length:
                colors.append(max_color)
            else:
                param = (value - min_length) / (max_length - min_length)
                colors.append(max_color * param + min_color * (1.0 - param))
            component.select = value is not None and value < min_length
        return colors

    def _projectTarget(self, point, normal, mesh):
        """project to target mesh. normals and closest"""
        if self.method() == "CLOSEST":
            result = mesh.closest_point_on_mesh(point)
        else:
            result = mesh.ray_cast(point, -normal)
        return (result[1] - point).length if result[0] else None

    def _projectSource(self, point, normal, skip_faces):
        """project to source mesh. normals only"""
        start_point = point
        while True:
            result = bpy.context.active_object.ray_cast(point - normal.normalized() * self.threshold(), -normal)
            if result[3] in skip_faces:
                point = result[1]
            else:
                break
        return (result[1] - start_point).length if result[0] else None

    @staticmethod
    def _paintFaces(colors):
        """paint faces of active object with colors"""
        color_layer = bpy.context.active_object.data.vertex_colors.active
        for color, face in zip(colors, bpy.context.active_object.data.polygons):
            for paint in face.loop_indices:
                color_layer.data[paint].color = color

    @staticmethod
    def _paintVertices(colors):
        """paint vertices of active object with colors"""
        if not colors:
            return
        color_layer = bpy.context.active_object.data.vertex_colors.active
        for face in bpy.context.active_object.data.polygons:
            for paint, vertex in zip(face.loop_indices, face.vertices):
                color_layer.data[paint].color = colors[vertex]
    

class Layout(bpy.types.Panel):
    bl_label = "AM 1D Xray++"
    bl_idname = "Andrey 1D Xray++"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = '1D'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        """col - main column layout. do not rewrite
        col_in - main column inside every section
        col_in_n - sub layout"""
        s = context.scene.am_xray_settings
        column = self.layout.column(align=True)
        column.prop(s, "xray_source", text="emitter")
        column.operator("mesh.am1dxr_xray_plus", text="apply")
        row_1 = column.row()
        row_1.prop(s, "xray_color_select", text="select")
        row_1 = column.row()
        row_1.prop(s, "xray_color_min", text="min")
        row_1 = column.row()
        row_1.prop(s, "xray_color_max", text="max")
        row_1 = column.row()
        row_1.prop(s, "xray_color_null", text="not found")
        column.prop(s, "xray_method", text="")
        column.prop(s, "xray_component", text="")
        column.prop(s, "xray_threshold", text="threshold")
        column.prop(s, "xray_min", text="min")
        column.prop(s, "xray_max", text="max")
        column.prop(s, "xray_max_auto", text="auto max")


def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.am_xray_settings = bpy.props.PointerProperty(type=Settings)


def unregister():
    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
